/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.oth.amp.ex1;

import org.camunda.bpm.engine.RuntimeService;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.UriBuilderException;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class OperatorRS {

    @Inject
    private RuntimeService runtimeService;

    @Context
    private UriInfo uriInfo;

    @POST
    @Path("submit")
    public void postTask(JsonObject task) {
        Map<String, Object> vars = new HashMap<>();
        vars.put("op", task.getString("op"));
        vars.put("x", task.getInt("x"));
        vars.put("y", task.getInt("y"));
        vars.put("callbackURI", task.getString("callbackURI", defaultCallbackURI()));
        runtimeService.startProcessInstanceByMessage("TaskMessage", vars);
    }

    private String defaultCallbackURI() throws IllegalArgumentException, UriBuilderException {
        return uriInfo.getBaseUriBuilder().segment("results").build().toString();
    }

}
