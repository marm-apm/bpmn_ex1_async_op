/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.oth.amp.ex1;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

@ApplicationScoped
@Named
public class Sender implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String callbackURI = (String) execution.getVariable("callbackURI");

        JsonObject result = Json.createObjectBuilder().
                add("op", (String) execution.getVariable("op")).
                add("x", (Integer) execution.getVariable("x")).
                add("y", (Integer) execution.getVariable("y")).
                add("result", ((Number) execution.getVariable("result")).doubleValue()).
                build();

        Client client = ClientBuilder.newClient();
        try {
            client.target(callbackURI).
                    request().
                    post(Entity.entity(result, MediaType.APPLICATION_JSON_TYPE));
        } finally {
            client.close();
        }
    }

}
