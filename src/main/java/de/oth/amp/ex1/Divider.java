package de.oth.amp.ex1;

import javax.ejb.Stateless;
import javax.inject.Named;
import org.camunda.bpm.engine.delegate.BpmnError;

/**
 *
 * @author mam02072
 */
@Stateless
@Named("divider")
public class Divider {

    public double apply(int x, int y) {
        return x / (double)y;
    }
    
    public double checkAndApply(int x, int y) {
        if (y == 0) {
            throw new BpmnError("DIV_100");
        }
        return x / (double)y;
    }

}
