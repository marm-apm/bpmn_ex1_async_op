/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oth.amp.ex1;

import org.camunda.bpm.engine.cdi.annotation.ProcessVariable;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

@Stateless
@Named
public class ErrorSender {

    public static final String MSG_DIV_BY_ZERO = "Division by Zero!";

    @Inject
    @ProcessVariable
    private Object callbackURI;

    public void sendBusy() {
        sendError("sorry, i'm very busy...");
    }

    public void sendDivByZero() {
        sendError(MSG_DIV_BY_ZERO);
    }

    private void sendError(String msg) {
        JsonObject result = Json.createObjectBuilder().
                add("error", msg).
                build();

        Client client = ClientBuilder.newClient();
        try {
            client.target(callbackURI.toString()).
                    request().
                    post(Entity.entity(result, MediaType.APPLICATION_JSON_TYPE));
        } finally {
            client.close();
        }
    }

}
